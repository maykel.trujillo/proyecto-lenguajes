﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProyectoLenguajes.Data.Migrations
{
    public partial class ModifyRelationBetweenOrderAndDishDetailsOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DishDetailsOrders_Orders_OrderId",
                table: "DishDetailsOrders");

            migrationBuilder.AlterColumn<int>(
                name: "OrderId",
                table: "DishDetailsOrders",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_DishDetailsOrders_Orders_OrderId",
                table: "DishDetailsOrders",
                column: "OrderId",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DishDetailsOrders_Orders_OrderId",
                table: "DishDetailsOrders");

            migrationBuilder.AlterColumn<int>(
                name: "OrderId",
                table: "DishDetailsOrders",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_DishDetailsOrders_Orders_OrderId",
                table: "DishDetailsOrders",
                column: "OrderId",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
