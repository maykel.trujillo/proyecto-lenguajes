﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoLenguajes.Data.Repository.IRepository
{
    public interface IUnitOfWork : IDisposable
    {
        IApplicationUserRepository ApplicationUser { get; }

        IDishRepository Dish { get; }

        IOrderRepository Order { get; }

        IDishDetailsOrderRepository DishDetailsOrder { get; }
        
        public void Save();
    }
}
