﻿using ProyectoLenguajes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoLenguajes.Data.Repository.IRepository
{
    public interface IApplicationUserRepository : IRepository<ApplicationUser>
    {
        bool Registration(ApplicationUser applicationUser);

        int Login(ApplicationUser applicationUser);

        int Update(ApplicationUser applicationUser);
    }
}
