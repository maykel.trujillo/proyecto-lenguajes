﻿using ProyectoLenguajes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoLenguajes.Data.Repository.IRepository
{
    public interface IDishDetailsOrderRepository : IRepository<DishDetailsOrder>
    {
        void Update(DishDetailsOrder dishDetails);
    }
}
