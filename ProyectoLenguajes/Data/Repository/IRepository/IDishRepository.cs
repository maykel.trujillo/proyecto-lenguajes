﻿using ProyectoLenguajes.Models;
using ProyectoLenguajes.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoLenguajes.Data.Repository.IRepository
{
    public interface IDishRepository : IRepository<Dish>
    {
        void Update(Dish dish);

        List<DishToListVM> DishesListToCustomer(string filter, int minPrice, int maxPrice);
    }
}
