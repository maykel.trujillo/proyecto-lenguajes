﻿using ProyectoLenguajes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoLenguajes.Data.Repository.IRepository
{
    public interface IOrderRepository : IRepository<Order>
    {
        void Update(Order order);

        Task Update(int order, string oldStatus);

        int getLastOrder(DateTime date, string customerId);
    }
}
