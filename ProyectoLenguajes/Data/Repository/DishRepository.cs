﻿using ProyectoLenguajes.Data.Repository.IRepository;
using ProyectoLenguajes.Models;
using ProyectoLenguajes.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoLenguajes.Data.Repository
{
    public class DishRepository : Repository<Dish>, IDishRepository
    {
        private readonly ApplicationDbContext _db;

        public DishRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public void Update(Dish dish)
        {
            var dishDb = _db.Dishes.FirstOrDefault(d => d.Id == dish.Id);
            if (dishDb != null)
            {
                dishDb.Name = dish.Name;
                dishDb.IsEnabled = dish.IsEnabled;
                dishDb.PhotoUrl = dish.PhotoUrl;
                dishDb.Price = dish.Price;
                dishDb.Description = dish.Description;
            }
        }

        public List<DishToListVM> DishesListToCustomer(string filter, int minPrice, int maxPrice) {
            
            IQueryable<DishToListVM> query = from d in _db.Set<Dish>()
                                        where d.IsEnabled
                                        select new DishToListVM { 
                                            Id = d.Id,
                                            Name = d.Name,
                                            Price = d.Price
                                        };
            if (filter != null)
            {
                query = query.Where(d => d.Name.Contains(filter));
            }
            query = query.Where(d => d.Price >= minPrice);
            if (maxPrice > 0)
            {
                query = query.Where(d => d.Price <= maxPrice);
            }

            return query.ToList();
        }
    }
}
