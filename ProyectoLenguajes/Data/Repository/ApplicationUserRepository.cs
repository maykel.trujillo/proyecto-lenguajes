﻿using ProyectoLenguajes.Data.Repository.IRepository;
using ProyectoLenguajes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoLenguajes.Data.Repository
{
    public class ApplicationUserRepository : Repository<ApplicationUser>, IApplicationUserRepository
    {
        private readonly ApplicationDbContext _db;

        public ApplicationUserRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public int Update(ApplicationUser applicationUser)
        {
            var user = _db.ApplicationUser.FirstOrDefault(u => u.Id == applicationUser.Id);
            if (user != null)
            {
                user.Name = applicationUser.Name;
                user.State = applicationUser.State;
                user.City = applicationUser.City;
                user.StreetAdress = applicationUser.StreetAdress;

                if (user.Email != applicationUser.Email)
                {
                    if (_db.ApplicationUser.FirstOrDefault(u => u.Email == applicationUser.Email) != null) //email is in use
                    {
                        return -2;
                    }
                    else
                    {
                        user.Email = applicationUser.Email;
                    }
                }

                return 1;
            }
            return -1;
        }

        public bool Registration(ApplicationUser applicationUser)
        {
            var user = _db.ApplicationUser.FirstOrDefault(u => u.NormalizedEmail == applicationUser.Email.ToUpper());
            if (user != null) //exists
            {
                return false;
            }
            applicationUser.NormalizedEmail = applicationUser.Email.ToUpper();
            applicationUser.UserName = applicationUser.Email.ToUpper();
            applicationUser.NormalizedUserName = applicationUser.Email.ToUpper();
            Add(applicationUser);
            return true;
        }

        public int Login(ApplicationUser applicationUser)
        {
            var user = _db.ApplicationUser.FirstOrDefault(u => u.NormalizedEmail == applicationUser.Email.ToUpper());
            if (user == null) //not exists
            {
                return -1;
            }

            if (applicationUser.Email == user.Email && applicationUser.PasswordHash == user.PasswordHash)
            {
                return 1;
            }
            return -2;
        }
    }
}
