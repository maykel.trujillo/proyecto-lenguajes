﻿using ProyectoLenguajes.Data.Repository.IRepository;
using ProyectoLenguajes.Models;
using ProyectoLenguajes.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoLenguajes.Data.Repository
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        private readonly ApplicationDbContext _db;

        public OrderRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public void Update(Order order)
        {
            var orderDb = _db.Orders.FirstOrDefault(o => o.Id == order.Id);
            if (orderDb != null)
            {
                orderDb.Status = order.Status;
            }
        }

        public async Task Update(int order, string oldStatus)
        {
            var orderDb = await _db.Orders.FindAsync(order);
            if (oldStatus == null)
            {
                orderDb.Status = StatusName.Status_Delivered;
            } else
            {
                switch (oldStatus)
                {
                    case StatusName.Status_Delayed:
                        orderDb.Status = StatusName.Status_Delayed;
                        break;
                    case StatusName.Status_On_Time:
                        orderDb.Status = StatusName.Status_On_Time;
                        break;
                    case StatusName.Status_Over_Time:
                        orderDb.Status = StatusName.Status_Over_Time;
                        break;
                    default:
                        orderDb.Status = StatusName.Status_Delayed;
                        break;
                }
            }
            await _db.SaveChangesAsync();

        }

        public int getLastOrder(DateTime date, string customerId)
        {
            return _db.Orders.FirstOrDefault(o => o.Date == date && o.ApplicationUserId == customerId).Id;
        }
    }
}
