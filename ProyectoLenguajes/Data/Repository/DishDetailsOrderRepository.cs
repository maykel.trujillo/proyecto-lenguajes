﻿using ProyectoLenguajes.Data.Repository.IRepository;
using ProyectoLenguajes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoLenguajes.Data.Repository
{
    public class DishDetailsOrderRepository : Repository<DishDetailsOrder>, IDishDetailsOrderRepository
    {
        private readonly ApplicationDbContext _db;

        public DishDetailsOrderRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }

        public void Update(DishDetailsOrder dishDetails)
        {
            var dishesDetailsDb = _db.DishDetailsOrders.FirstOrDefault(d => d.Id == dishDetails.Id);
            if (dishesDetailsDb != null)
            {
                dishesDetailsDb.Name = dishDetails.Name;
                dishesDetailsDb.Amount = dishDetails.Amount;
                dishesDetailsDb.TotalPrice = dishDetails.TotalPrice;
            }
        }
    }
}
