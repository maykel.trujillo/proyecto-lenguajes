﻿using ProyectoLenguajes.Data.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoLenguajes.Data.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _db;

        public IApplicationUserRepository ApplicationUser { get; set; }

        public IDishRepository Dish { get; set; }

        public IOrderRepository Order { get; set; }

        public IDishDetailsOrderRepository DishDetailsOrder { get; set; }

        public UnitOfWork(ApplicationDbContext db)
        {
            _db = db;
            ApplicationUser = new ApplicationUserRepository(_db);
            Dish = new DishRepository(_db);
            Order = new OrderRepository(_db);
            DishDetailsOrder = new DishDetailsOrderRepository(_db);
        }

        public void Dispose()
        {
            _db.Dispose();
        }

        public void Save()
        {
            _db.SaveChanges();
        }
    }
}
