﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ProyectoLenguajes.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoLenguajes.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<ApplicationUser> ApplicationUser { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<Dish> Dishes { get; set; }

        public DbSet<DishDetailsOrder> DishDetailsOrders { get; set; }

    }
}
