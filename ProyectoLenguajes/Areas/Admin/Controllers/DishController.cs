﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using ProyectoLenguajes.Data.Repository.IRepository;
using ProyectoLenguajes.Models;
using ProyectoLenguajes.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoLenguajes.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = RoleName.Role_Admin)]
    public class DishController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWebHostEnvironment _hostEnvioronment;

        public DishController(IUnitOfWork unitOfWork, IWebHostEnvironment hostEnvironment)
        {
            _unitOfWork = unitOfWork;
            _hostEnvioronment = hostEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Upsert(int? id)
        {
            Dish dish = new Dish();
            if (id == null) //create
            {
                return View(dish);
            }
            dish = _unitOfWork.Dish.Get(id.GetValueOrDefault());

            if (dish == null)
            {
                return NotFound();
            }
            return View(dish);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Upsert(Dish dish)
        {
            if (ModelState.IsValid)
            {
                string webRootPath = _hostEnvioronment.WebRootPath;
                var files = HttpContext.Request.Form.Files; //obtener direccion de archivos

                if (files.Count > 0)
                {
                    //si se agregó una imagen, hay que eliminar la direccion anterior del producto
                    Dish dishDB = _unitOfWork.Dish.Get(dish.Id);
                    if (dishDB != null)
                    {
                        var imagePath = Path.Combine(webRootPath, dishDB.PhotoUrl.TrimStart('\\'));

                        if (System.IO.File.Exists(imagePath))
                        {
                            System.IO.File.Delete(imagePath);//eliminar archivo existente
                        }
                    }

                    string fileName = Guid.NewGuid().ToString();
                    var uploads = Path.Combine(webRootPath, @"images\dishes");
                    var extension = Path.GetExtension(files[0].FileName);

                    dish.PhotoUrl = @"\images\dishes\" + fileName + extension;

                    using (var fileStreams = new FileStream(Path.Combine(uploads, fileName + extension),
                                FileMode.Create))
                    {

                        files[0].CopyTo(fileStreams);
                    } //para escribir un archivo
                }
                else //no changes in image
                {
                    if (dish.Id != 0)//verificar que es un update
                    {
                        Dish dishDB = _unitOfWork.Dish.Get(dish.Id);
                        dish.PhotoUrl = dishDB.PhotoUrl;
                    }
                }

                if (dish.Id == 0) //insert
                {
                    _unitOfWork.Dish.Add(dish);
                }
                else
                {
                    _unitOfWork.Dish.Update(dish);
                }
                _unitOfWork.Save();
                return RedirectToAction(nameof(Index));
            }

            return View(dish);
        }

        #region API CALLS

        [HttpGet]
        public IActionResult GetAll()
        {
            var allObjetcs = _unitOfWork.Dish.GetAll();
            return Json(new { data = allObjetcs });
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            var dish = _unitOfWork.Dish.Get(id);

            if (dish == null)
            {
                return Json(new { success = false, message = "Error while deleting" });
            }
            string webRootPath = _hostEnvioronment.WebRootPath;
            var imagePath = Path.Combine(webRootPath, dish.PhotoUrl.TrimStart('\\'));

            if (System.IO.File.Exists(imagePath))
            {
                System.IO.File.Delete(imagePath);//eliminar archivo existente
            }

            _unitOfWork.Dish.Remove(dish);
            _unitOfWork.Save();
            return Json(new { success = true, message = "Deleted successfully" });
        }

        #endregion
    }
}
