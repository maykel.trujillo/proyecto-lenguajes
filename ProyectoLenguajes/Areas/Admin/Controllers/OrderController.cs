﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using ProyectoLenguajes.Data.Repository.IRepository;
using ProyectoLenguajes.Models;
using Microsoft.AspNetCore.Authorization;
using ProyectoLenguajes.Utility;
using System.Threading;

namespace ProyectoLenguajes.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = RoleName.Role_Admin)]
    public class OrderController:Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWebHostEnvironment _hostEnvioronment;

        public OrderController(IUnitOfWork unitOfWork, IWebHostEnvironment hostEnvironment)
        {
            _unitOfWork = unitOfWork;
            _hostEnvioronment = hostEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult EditOrder(int? id)
        {
            Order order = new Order();
            if (id == null) 
            {
                return View(order);
            }
            order = _unitOfWork.Order.Get(id.GetValueOrDefault());
            order.ApplicationUser = _unitOfWork.ApplicationUser.GetAll().Where(I => I.Id == order.ApplicationUserId).FirstOrDefault();

            if (order == null)
            {
                return NotFound();
            }
            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditOrder(Order order)
        {
            _unitOfWork.Order.Update(order);
            _unitOfWork.Save();
            return RedirectToAction(nameof(Index));
        }

        #region API CALLS

        [HttpGet]
        public IActionResult GetAll()
        {
            var allObjetcs = _unitOfWork.Order.GetAll();
            foreach (var item in allObjetcs)
            {
                item.ApplicationUser = _unitOfWork.ApplicationUser.GetAll().Where(I=>I.Id == item.ApplicationUserId).FirstOrDefault();
            }
            return Json(new { data = allObjetcs });
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            var order = _unitOfWork.Order.Get(id);

            if (order == null)
            {
                return Json(new { success = false, message = "Error while deleting" });
            }
            _unitOfWork.Order.Remove(order);
            _unitOfWork.Save();
            return Json(new { success = true, message = "Deleted successfully" });
        }
        #endregion
    }
}