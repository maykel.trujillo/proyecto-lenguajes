﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using ProyectoLenguajes.Data;
using ProyectoLenguajes.Data.Repository.IRepository;
using ProyectoLenguajes.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using ProyectoLenguajes.Utility;

namespace ProyectoLenguajes.Areas.Admin.Controllers
{
   
    [Area("Admin")]
    [Authorize(Roles = RoleName.Role_Admin)]
    public class CustomerController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWebHostEnvironment _hostEnvioronment;

        public CustomerController(IUnitOfWork unitOfWork, IWebHostEnvironment hostEnvironment)
        {
            _unitOfWork = unitOfWork;
            _hostEnvioronment = hostEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Upsert(int? id)
        {
   
            ApplicationUser user = new ApplicationUser();
            if (id == null) //create
            {
                return View(user);
            }
            user = _unitOfWork.ApplicationUser.Get(id.GetValueOrDefault());

            if (user == null)
            {
                return NotFound();
            }
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Upsert(ApplicationUser user)
        {
            if (ModelState.IsValid)
            {
                if (user.Id == "")
                { //insert
                    _unitOfWork.ApplicationUser.Add(user);
                }
                else
                {//edit
                    _unitOfWork.ApplicationUser.Update(user);
                }
                _unitOfWork.Save();
                //redirecciona a index
                return RedirectToAction(nameof(Index));
            }
            //retorna vista actual con el objeto actual 
            return View(user);
        }

            #region API CALLS

            [HttpGet]
        public IActionResult GetAll()
        {
            List<ApplicationUser> users = new List<ApplicationUser>();
            var allObjetcs = _unitOfWork.ApplicationUser.GetAll();
            foreach (var item in allObjetcs)
            {
                if ((item.Role != "Admin") && (item.Role != "Cook"))
                    users.Add(item);
            }
            return Json(new { data = users });
        }

        [HttpDelete]
        public IActionResult Delete(int id)
        {
            var user = _unitOfWork.ApplicationUser.Get(id);

            if (user == null)
            {
                return Json(new { success = false, message = "Error while deleting" });
            }

            _unitOfWork.ApplicationUser.Remove(user);
            _unitOfWork.Save();
            return Json(new { success = true, message = "Deleted Successfully" });
        }


        [HttpPost]
        public IActionResult LockUnlock([FromBody] string id)
        {
            //var objFromDb = _unitOfWork.ApplicationUser.FirstOrDefault(u => u.Id == id);
            var objFromDb = _unitOfWork.ApplicationUser.GetFirstOrDefault(u => u.Id == id);

            if (objFromDb == null)
            {
                return Json(new { success = false, message = "Error while Locking/Unlocking" });
            }

            if (objFromDb.LockoutEnd != null && objFromDb.LockoutEnd > DateTime.Now)
            {
                objFromDb.LockoutEnd = DateTime.Now;
            }
            else
            {
                objFromDb.LockoutEnd = DateTime.Now.AddYears(100);
            }

            _unitOfWork.Save();

            return Json(new { success = true, message = "Operation Successfull" });

        }

        #endregion
    }
}
