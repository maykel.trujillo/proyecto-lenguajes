﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using ProyectoLenguajes.Data;
using ProyectoLenguajes.Data.Repository.IRepository;
using ProyectoLenguajes.Models;
using ProyectoLenguajes.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoLenguajes.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = RoleName.Role_Admin)]
    public class UserController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWebHostEnvironment _hostEnvioronment;

        public UserController(IUnitOfWork unitOfWork, IWebHostEnvironment hostEnvironment)
        {
            _unitOfWork = unitOfWork;
            _hostEnvioronment = hostEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Upsert(string? id)
        {

            ApplicationUser user = new ApplicationUser();
            if (id == null) //create
            {
                return View(user);
            }
            user = _unitOfWork.ApplicationUser.Get(id);

            if (user == null)
            {
                return NotFound();
            }
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Upsert(ApplicationUser user)
        {
            if (ModelState.IsValid)
            {
                if (user.Id == "")
                { //insert
                    _unitOfWork.ApplicationUser.Add(user);
                }
                else
                {//edit
                    _unitOfWork.ApplicationUser.Update(user);
                }
                _unitOfWork.Save();
                //redirecciona a index
                return RedirectToAction(nameof(Index));
            }
            //retorna vista actual con el objeto actual 
            return View(user);
        }

        #region API CALLS

        [HttpGet]
        public IActionResult GetAll()
        {
            List<ApplicationUser> users = new List<ApplicationUser>();
            var allObjetcs = _unitOfWork.ApplicationUser.GetAll();

            foreach (var item in allObjetcs)
            {
                if ((item.Role == "Admin") || (item.Role == "Cook"))
                    users.Add(item);
            }
            return Json(new { data = users });

        }
    
 
    


[HttpDelete]
        public IActionResult Delete(string id)
        {
            var user = _unitOfWork.ApplicationUser.Get(id);

            if (user == null)
            {
                return Json(new { success = false, message = "Error while deleting" });
            }

            _unitOfWork.ApplicationUser.Remove(user);
            _unitOfWork.Save();
            return Json(new { success = true, message = "Deleted Successfully" });
        }

        #endregion
    }
}
