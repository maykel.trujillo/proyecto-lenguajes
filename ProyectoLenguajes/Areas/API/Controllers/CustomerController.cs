﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProyectoLenguajes.Data.Repository.IRepository;
using ProyectoLenguajes.Models;
using ProyectoLenguajes.Models.ViewModels;
using ProyectoLenguajes.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoLenguajes.Areas.Customer.Controllers
{ 
    [EnableCors]
    [Area("API")]
    public class CustomerController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public CustomerController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #region Dish List
        /// <summary>
        /// Gets all dishes that are enabeld
        /// </summary>
        /// <returns>Dish list in json and success condition</returns>
        [HttpGet]
        public IActionResult Index(string filter, int minPrice, int maxPrice)
        {
            var allDishes = _unitOfWork.Dish.DishesListToCustomer(
                filter, 
                minPrice,
                maxPrice);
            return Json(new { data = allDishes, success = true });
        }
        #endregion

        #region Dish

        /// <summary>
        /// Gets dish's description
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Dish's description in json and success condition</returns>
        [HttpGet]
        public IActionResult GetDishDescription(int id)
        {
            Dish dish = _unitOfWork.Dish.Get(id);

            if (dish == null)
            {
                return Json(new { data = "The indicated dish has not been found", success = false });
            }

            return Json(new { data = dish, success = true });
        }

        [HttpGet]
        public IActionResult GetDishImage(string url)
        {
            var fullUrl = Path.GetFullPath("wwwroot" + url);
            return new FileStreamResult(new FileStream(fullUrl, FileMode.Open), "image/jpeg");
        }

        #endregion

        #region Order
        /// <summary>
        /// Saves a customer's order in data base in status "On Time"
        /// </summary>
        /// <param name="dishesId">id from dishes that order contains</param>
        /// <param name="customerId">customer id</param>
        /// <returns>success condition and message</returns>
        [HttpPost]
        public IActionResult SaveOrder(string dishes, string customerId) {

            if (dishes == null)
            {
                return Json(new { data = "No dishes have been added", success = false });
            }
            if (customerId == null)
            {
                return Json(new { data = "The client is not recognized", success = false });
            }

            Order newOrder = new Order()
            {
                ApplicationUserId = customerId,
                Status = StatusName.Status_On_Time,
                Date = DateTime.Now
            };
            _unitOfWork.Order.Add(newOrder);
            _unitOfWork.Save();

            JArray jDishes = JArray.Parse(dishes);

            foreach (var item in jDishes)
            {
                DishDetailsOrder dishDetails = JsonConvert.DeserializeObject<DishDetailsOrder>(item.ToString());
                dishDetails.Id = 0;
                dishDetails.OrderId = _unitOfWork.Order.getLastOrder(newOrder.Date, customerId);
                _unitOfWork.DishDetailsOrder.Add(dishDetails);
            }
            _unitOfWork.Save();
            return Json(new { data = "Order sent successfully", success = true });
        }
        #endregion

        #region User Profile
        [HttpGet]
        public IActionResult GetProfile(string customerId)
        {
            var user = _unitOfWork.ApplicationUser.GetFirstOrDefault(u => u.Id == customerId);

            if (user == null)
            {
                return Json(new { data = "The client is not recognized", success = false });
            }

            return Json(new { data = user, success = true });
        }

        [HttpPost]
        public IActionResult Registration(string profile)
        {
            ApplicationUser user = JsonConvert.DeserializeObject<ApplicationUser>(profile);

            //validation of spaces entered
            if (user.Name == "")
            {
                return Json(new { data = "First enter a name", success = false });
            }
            if (user.State == "")
            {
                return Json(new { data = "First enter a state", success = false });
            }
            if (user.City == "")
            {
                return Json(new { data = "First enter a city", success = false });
            }
            if (user.StreetAdress == "")
            {
                return Json(new { data = "First enter a street address", success = false });
            }

            //email and password validation
            if (!user.Email.Equals(user.ConfirmEmail))
            {
                return Json(new { data = "The email entered does not match", success = false });
            }
            if (!user.PasswordHash.Equals(user.ConfirmPassword))
            {
                return Json(new { data = "The password entered does not match", success = false });
            }
            
            if (_unitOfWork.ApplicationUser.Registration(user))
            {
                _unitOfWork.Save();
                return Json(new { data = "Account created successfully", success = true, token = user.Id});
            }
            else
            {
                return Json(new { data = "The email entered is already registered in the system", success = false });
            }
        }

        [HttpPost]
        public IActionResult Login(string profile)
        {
            ApplicationUser user = JsonConvert.DeserializeObject<ApplicationUser>(profile);

            int condition = _unitOfWork.ApplicationUser.Login(user);

            return condition switch
            {
                1 => Json(new { data = "Successful login", success = true, 
                    token = _unitOfWork.ApplicationUser.GetFirstOrDefault(u => u.NormalizedEmail 
                    == user.Email.ToUpper()).Id }),
                -1 => Json(new { data = "The email entered is not registered in the system", success = false }),
                -2 => Json(new { data = "The password entered is incorrect", success = false }),
                _ => Json(new { data = "Unknown error", success = false }),
            };
        }

        [HttpPost]
        public IActionResult UpdateProfile(string profile) {
            ApplicationUser user = JsonConvert.DeserializeObject<ApplicationUser>(profile);

            switch (_unitOfWork.ApplicationUser.Update(user))
            {
                case 1:
                    _unitOfWork.Save();
                    return Json(new { data = "User profile updated successfully", success = true });
                case -1: return Json(new { data = "The client is not recognized", success = false });
                case -2: return Json(new { data = "The mail entered is already in use", success = false });
                default: return Json(new { data = "Unknown error", success = false });
            }
        }

        #endregion
    }
}
