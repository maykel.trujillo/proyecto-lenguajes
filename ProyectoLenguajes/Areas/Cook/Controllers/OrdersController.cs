﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProyectoLenguajes.Data;
using ProyectoLenguajes.Data.Repository.IRepository;
using ProyectoLenguajes.Models;
using ProyectoLenguajes.Models.ViewModels;
using ProyectoLenguajes.Utility;

namespace ProyectoLenguajes.Areas.Cook.Controllers
{
    [Area("Cook")]
    public class OrdersController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public OrdersController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: Cook/Orders
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult GetOrders()
        {

            IEnumerable<Order> orderList = _unitOfWork.Order.GetAll(
                includeProperties: "ApplicationUser",
                filter: (o => !o.Status.Equals(StatusName.Status_Delivered) && !o.Status.Equals(StatusName.Status_Annulled)));
            
            List<OrderViewModel> orderViews = new List<OrderViewModel>();

            foreach (var order in orderList)
            {
                OrderViewModel orderV = new OrderViewModel();
                orderV.custumer = order.ApplicationUser.Name;
                orderV.Status = order.Status;
                orderV.Id = order.Id;

                orderV.dishes = _unitOfWork.DishDetailsOrder.GetAll(filter: o => o.OrderId == order.Id);
                orderViews.Add(orderV);
            }
            return Json(new { data = orderViews });
        }

        // POST: Cook/Orders/Edit/
        [HttpPost]
        public async Task<IActionResult> CompleteOrder(int id)
        {
            await _unitOfWork.Order.Update(id, null);
            return Json(new { data = "Order completed" });
        }

        [HttpPost]
        public async Task<IActionResult> undoOrderCompleted(int id, string oldStatus) {
            await _unitOfWork.Order.Update(id, oldStatus);
            return Json(new { data = "The last completed plate has been returned" });
        }

    }
}
