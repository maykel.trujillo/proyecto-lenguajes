﻿//show alerts 
function showAlert(message, type) {
    if (message != null) {
        if (type === 'False') {
            toastr.error(message, 'Error', { "positionClass": "toast-bottom-right" });
        } else {
            toastr.success(message, 'Éxito', { "positionClass": "toast-bottom-right" });
        }
    }
}