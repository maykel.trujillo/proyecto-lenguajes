﻿let orders; //list of orders from server
let containerOrders; //div that contains visual orders
var isRemoveOrder = false;//indicates that a dish has removed
let lastRemovedId;//id from last order removed
let oldStatus;//status from last order removed

$(document).ready(() => {
    getOrders();
    setInterval(getOrders,10000);
});


function getOrders() {
    containerOrders = $('#Cards');
    containerOrders.children().remove();
    
    $.ajax({
        url: '/Cook/Orders/GetOrders',
        type: 'GET',
        dataType: 'json',
        success: function (resp) {
            orders = Array.from(resp.data);

            if (orders.length > 6) {
                var amount = orders.length - 6;
                if (document.getElementById("amountIndicator") != null) {
                    document.getElementById("amountIndicator").remove();
                }
                $('#orderIndicator').append(`
                    <h2 class="text-info" id="amountIndicator">There are <strong>${amount}</strong> more dishes in queue...</h2>
                `);
            } else {
                if (document.getElementById("amountIndicator") != null) {
                    document.getElementById("amountIndicator").remove();
                }
            }
            for (var i = 0; i < orders.length && i < 6; i++) {
                var typeOrder;
                if (orders[i].status == "On Time") {
                    typeOrder = "onTime";
                } else if (orders[i].status == "Delayed") {
                    typeOrder = "delayed";
                } else {
                    typeOrder = "overTime";
                }
                containerOrders.append(`
                    <div class="card col-2 basicOrder ${typeOrder}" style="border:0px; " id="order${orders[i].id}">
                        <div>
                            <div>
                                <p class="card-title h5"><b>${i + 1} - ${orders[i].custumer}</b></p>
                            </div>
                            <div class="pl-1" id="${orders[i].id}">
                                <p class="card-title">
                                </p>
                            </div>
                        </div>
                        <button type="button" class="btn" onclick="removeOrder('${orders[i].id}','${orders[i].status}')">Ready</button>
                    </div>
                `);
                addDishDetails(orders[i].id, orders[i].dishes);
            }
        }
    });
}

function addDishDetails(id, dishes) {
    var dishesContainer = $('#'+id);
    for (var i = 0; i < dishes.length; i++) {
        dishesContainer.append(`
        <p>${dishes[i].name} ${dishes[i].amount}<br></p>
    `);
    }
}

function removeOrder(id,status) {
    //document.getElementById("order" + id).hidden = true;
    lastRemovedId = id;
    oldStatus = status;
    if (!isRemoveOrder) {
        $('#undoIndicator').append(`
            <button id="buttonUndo" onclick="undo()" class="btn btn-info col-2 m-3" style="font-size:2em; padding: 0.5em 0 0.5em 0" title="Return the last plate removed">Undo</button>
        `);
        isRemoveOrder = true;
    }
    $.ajax({
        data: {id:id},
        url: '/Cook/Orders/CompleteOrder',
        type: 'POST',
        dataType: 'json',
        success: function (resp) {
            getOrders();
            toastr.success(resp.data);
        }
    });
}

function undo() {
    //document.getElementById("order" + lastRemovedId).hidden = false;
    document.getElementById("buttonUndo").remove();
    isRemoveOrder = false;

    $.ajax({
        data: {
            id: lastRemovedId,
            oldStatus:oldStatus
        },
        url: '/Cook/Orders/undoOrderCompleted',
        type: 'POST',
        dataType: 'json',
        success: function (resp) {
            getOrders();
            toastr.success(resp.data);
        }
    });
}