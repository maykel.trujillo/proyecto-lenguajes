﻿var dataTable;

$(document).ready(function () {
    loadDataTable();
});


function loadDataTable() {
    dataTable = $('#tblData').DataTable({
        "ajax": {
            "url": "/Admin/Customer/GetAll"
        },
        "columns": [
            { "data": "name", "width": "15%" },
            { "data": "state", "width": "15%" },
            { "data": "city", "width": "15%" },
            { "data": "streetAdress", "width": "15%" },
            {
                "data": {id: "id", lockoutEnd: "lockoutEnd"},
                "render": function (data) {
                    var today = new Date().getTime();
                    var lockOut = new Date(data.lockoutEnd).getTime();
                    if (lockOut > today) //User is locked
                    {
                        return `
                                <div class="text-center">
                                    <a onclick=LockUnlock('${data.id}') class="btn btn-danger text-white" style="cursor:pointer; width:100px">
                                        <i class="fas fa-lock-open"></i> Unlock
                                    </a>
                                </div>
                               `;
                    }
                    //User is Unlocked
                    return `
                                    <div class="text-center">
                                    <a onclick=LockUnlock('${data.id}') class="btn btn-success text-white" style="cursor:pointer; width:100px">
                                        <i class="fas fa-lock"></i> Lock
                                    </a>
                                </div>
                           `;
                }, "width": "15%"
            }
        ]

    });
}



function LockUnlock(id) {
            $.ajax({
                type: "POST",
                url: '/Admin/Customer/LockUnlock',
                data: JSON.stringify(id),
                contentType: "application/json",
                success: function (data) {
                    if (data.success) {
                        toastr.success(data.message);
                        dataTable.ajax.reload();
                    }
                    else {
                        toastr.error(data.message);    
                    }
                }
            });
 }
