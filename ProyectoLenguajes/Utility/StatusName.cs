﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoLenguajes.Utility
{
    public class StatusName
    {
        public const string Status_On_Time = "On Time";

        public const string Status_Over_Time = "Over Time";

        public const string Status_Delayed = "Delayed";

        public const string Status_Delivered = "Delivered";

        public const string Status_Annulled = "Annulled";

    }
}
