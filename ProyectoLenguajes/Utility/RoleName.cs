﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoLenguajes.Utility
{
    public class RoleName
    {
        public const string Role_Customer = "Customer";

        public const string Role_Admin = "Admin";

        public const string Role_Cook = "Cook";
    }
}
