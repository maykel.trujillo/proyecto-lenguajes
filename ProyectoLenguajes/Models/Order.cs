﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoLenguajes.Models
{
    public class Order
    {
        public int Id { get; set; }

        [Required]
        public string Status { get; set; }

        [Required]
        public string ApplicationUserId { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [ForeignKey("ApplicationUserId")]
        public ApplicationUser ApplicationUser { get; set; }

    }
}
