﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoLenguajes.Models
{
    public class DishDetailsOrder
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public double TotalPrice { get; set; }

        [Required]
        public int Amount { get; set; }

        [Required]
        public int OrderId { get; set; }

        [ForeignKey("OrderId")]
        public Order Order { get; set; }
    }
}
