﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoLenguajes.Models
{
    public class Dish
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public double Price { get; set; }

        [Display(Name="Photo")]
        public string PhotoUrl { get; set; }

        [Required]
        [Display(Name="Enabled")]
        public bool IsEnabled { get; set; }

    }
}
