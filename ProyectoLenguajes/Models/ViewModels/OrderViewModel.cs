﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoLenguajes.Models.ViewModels
{
    public class OrderViewModel
    {
        public int Id { get; set; }

        public string Status { get; set; }

        public string custumer { get; set; }

        public IEnumerable<DishDetailsOrder> dishes { get; set; }
    }
}
